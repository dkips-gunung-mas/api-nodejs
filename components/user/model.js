const mongoose = require('mongoose')
const crypto = require('crypto')
const env = require('../../env')

const Schema = new mongoose.Schema(
    {
        firstName: { type: String },
        lastName: { type: String },
        username: { type: String, unique: true },
        password: { type: String },
        email: { type: String, unique: true },
        verificationCode: { type: String },
        verifiedAt: { type: Date },
    },
    {
        timestamps: true,
    },
)

Schema.methods.validatePassword = function (password) {
    const _password = crypto
        .pbkdf2Sync(password, env.salt, 10000, 32, 'sha512')
        .toString('hex')

    return this.password === _password
}

Schema.methods.setPassword = function (password) {
    this.password = crypto
        .pbkdf2Sync(password, env.salt, 10000, 32, 'sha512')
        .toString('hex')
}

module.exports = mongoose.model('User', Schema, 'users')