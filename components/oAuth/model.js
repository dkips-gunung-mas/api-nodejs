const AccessToken = require('./AccessToken')
const Client = require('./Client')
const Code = require('./Code')
const User = require('../user/model')

exports.getAccessToken = async (accessToken) => {
    let _accessToken = await AccessToken.findOne({ accessToken: accessToken })
        .populate('user')
        .populate('client');

    if (!_accessToken) {
        return false;
    }

    _accessToken = _accessToken.toObject();

    if (!_accessToken.user) {
        _accessToken.user = {};
    }
    return _accessToken;
};

exports.getRefreshToken = (refreshToken) => {
    return AccessToken.findOne({ refreshToken: refreshToken })
        .populate('user')
        .populate('client');
};

exports.getAuthorizationCode = (code) => {
    return Code.findOne({ authorizationCode: code })
        .populate('user')
        .populate('client');
};

exports.getClient = (clientId, clientSecret) => {
    let params = { clientId: clientId };
    if (clientSecret) {
        params.clientSecret = clientSecret;
    }
    return Client.findOne(params);
};

exports.getUser = async (username, password) => {
    let user = await User.findOne({ username: username });
    if (user.validatePassword(password)) {
        return user;
    }
    return false;
};

exports.getUserFromClient = (client) => {
    // let User = mongoose.model('User');
    // return User.findById(client.user);
    return {};
};

exports.saveToken = async (token, client, user) => {
    let accessToken = (await AccessToken.create({
        user: user.id || null,
        client: client.id,
        accessToken: token.accessToken,
        accessTokenExpiresAt: token.accessTokenExpiresAt,
        refreshToken: token.refreshToken,
        refreshTokenExpiresAt: token.refreshTokenExpiresAt,
        scope: token.scope,
    })).toObject();

    if (!accessToken.user) {
        accessToken.user = {};
    }

    return accessToken;
};

exports.saveAuthorizationCode = (code, client, user) => {
    let authCode = new Code({
        user: user.id,
        client: client.id,
        authorizationCode: code.authorizationCode,
        expiresAt: code.expiresAt,
        scope: code.scope
    });
    return authCode.save();
};

exports.revokeToken = async (accessToken) => {
    let result = await AccessToken.deleteOne({
        refreshToken: accessToken.refreshToken
    });
    return result.deletedCount > 0;
};

exports.revokeAuthorizationCode = async (code) => {
    let result = await Code.deleteOne({
        authorizationCode: code.authorizationCode
    });
    return result.deletedCount > 0;
};