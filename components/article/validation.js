const { validationResult } = require('express-validator')

module.exports = function (request, response, next) {
    const errors = validationResult(request)

    if (errors.isEmpty() === false) {
        response.status(422).send({ errors: errors.array() })
        
        return  
    }
    
    next()
}