const express = require('express')
const rules = require('./rules')
const validation = require('./validation')
const { index, create, read, update, destroy } = require('./controller')

const router = express.Router()

// router.post('/', indexPost)
router.get('/', index)
router.post('/', rules, validation, create)
router.get('/:id', read)
router.put('/:id', update)
router.delete('/:id', destroy)

module.exports = router