const Model = require('./model')

exports.index = async function (request, response) {
    const page = parseInt(request.query.page || 1)
    const perPage = parseInt(request.query.per_page || 10)
    
    let query = {}

    if (request.query.title) {
        query.title = { $regex : new RegExp(request.query.title, 'i') }
    }

    let sort = {}

    if (request.query.sort) {
        // Array destrucuring
        const [name, direction = 'asc'] = request.query.sort.split(',')
        // const name = request.query.sort.split(',')[0]
        // const direction = request.query.sort.split(',')[1]
        
        if (direction === 'asc') {
            sort[name] = 1
        } else if (direction === 'desc') {
            sort[name] = -1
        }
        
        // switch (direction) {
        //     case 'asc':
        //         sort[name] = 1
        //         break
        //     case 'desc':
        //         sort[name] = -1
        //         break
        //     default:
        //         sort[name] = 1
        //         break
        // }
    }

    const docs = await Model
        .find(query)
        .sort(sort)
        .skip((page - 1) * perPage)
        .limit(perPage)

    response.send(docs)
}

exports.indexPost = async function (request, response) {
    const page = parseInt(request.body.page || 1)
    const perPage = parseInt(request.body.per_page || 10)
    
    let query = {}

    if (request.body.title) {
        query.title = { $regex : new RegExp(request.query.title, 'i') }
    }

    let sort = {}

    if (request.body.sort) {
        const [name, direction = 'asc'] = request.body.sort.split(',')
        
        if (direction === 'asc') {
            sort[name] = 1
        } else {
            sort[name] = -1
        }
    }

    const docs = await Model
        .find(query)
        .sort(sort)
        .skip((page - 1) * perPage)
        .limit(perPage)

    response.send(docs)
}

exports.create = async function (request, response) {
    const doc = new Model({
        title: request.body.title,
        content: request.body.content,
    })

    const data = await doc.save()

    response.send(data)
}

exports.read = async function (request, response) {
    const data = await Model.findById(request.params.id)

    response.send(data)
}

exports.update = async function (request, response) {
    const data = await Model.findByIdAndUpdate(
        request.params.id,
        {
            title: request.body.title,
            content: request.body.content,
        },
        // Mengembalikan data setelah diupdate
        { new: true }
    )

    response.send(data)
}

exports.destroy = async function (request, response) {
    await Model.findByIdAndRemove(request.params.id)    

    response.send({ ok: true })
}
