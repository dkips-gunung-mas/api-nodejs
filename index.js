require('./config/db')
const express = require('express')
const bodyParser = require('body-parser')
const OAuthServer = require('express-oauth-server')
const OAuthModel = require('./components/oAuth/model')
const User = require('./components/user/model')
const OAuthClient = require('./components/oAuth/Client')
const crypto = require('crypto')

const PORT = 3000
const oauth = new OAuthServer({
    model: OAuthModel,
    debug: true,
})
const app = express()

app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(express.static('public'))
app.use('/articles', require('./components/article/routes'))

app.post('/oauth/access_token', oauth.token({
    requireClientAuthentication: {
        authorization_code: false
    }
}))

app.post('/register', async (req, res, next) => {
    let _user = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        username: req.body.username,
        email: req.body.email,
        verificationCode: crypto.randomBytes(16).toString('hex'),
    })
    _user.setPassword(req.body.password)
    let user = null
    
    try {
        user = await _user.save()
    } catch (error) {
        return res.status(422).send(error.errmsg)
    }

    if (!user) {
        return res.send('Error creating user', 422)
    }

    // Create OAuth Client
    let _client = await OAuthModel.getClient(
        req.body.clientId,
        req.body.clientSecret
    )

    if (!_client) {
        _client = new OAuthClient({
            user: user.id,
            clientId: req.body.clientId,
            clientSecret: req.body.clientSecret,
            // redirectUris: req.body.redirectUris.split(','),
            grants: ['authorization_code', 'client_credentials',
                'refresh_token', 'password']
        })
        _client.save()
    }

    return res.send({ ok: true })
})

app.use('/restricted', oauth.authenticate(), (req, res) => {
    return res.send(res.locals)
});

app.listen(PORT, () => console.info(`Example app listening on port ${PORT}!`))
