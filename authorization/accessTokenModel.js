let userHelper
let accessTokenHelper

module.exports = function (injectedUserHelper, injectedAccessTokenHelper) {
    userHelper = injectedUserHelper
    accessTokenHelper = injectedAccessTokenHelper

    return {
        getClient,
        saveAccessToken,
        getUser,
        grantTypeAllowed,
        getAccessToken,
    }
}

function getClient(clientID, clientSecret, callback){
    const client = {
        clientID,
        clientSecret,
        grants: null,
        redirectUris: null
    }

    callback(false, client);
}

function grantTypeAllowed(clientID, grantType, callback) {
    console.log({ clientID, grantType });
    callback(false, true);
}

function getUser(username, password, callback){
    console.log({ username, password, callback });
    userHelper.getFromCredentials(username, password, callback)
}

function saveAccessToken(accessToken, clientID, expires, user, callback){
    console.log({ accessToken, clientID, user, accessToken })
    accessTokenHelper.saveAccessToken(accessToken, user.id, callback)
}

function getAccessToken(bearerToken, callback) {
    accessTokenHelper.getUserIdFromBearerToken(bearerToken, function (userID) {
        const accessToken = {
            user: { id: userID },
            expires: null
        }

        callback(
            userID == null ? true : false,
            userID == null ? null : accessToken
        )
    })
}

// function saveRefreshToken (refresh_token, callback) {
//     accessTokenHelper.saveRefreshToken(refresh_token, user.id, callback)
// }
