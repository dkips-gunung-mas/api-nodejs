let dbConnection

module.exports = function (injectedDbConnection) {
    dbConnection = injectedDbConnection

    return {
        register,
        getFromCredentials,
        exists
    }
}

function register (username, password, callback) {
    const query = `
        INSERT INTO users (username, password)
        VALUEs ('${username}', SHA('${password}'))
    `

    dbConnection.query(query, callback)
} 

function getFromCredentials (username, password, callback) {
    const query = `
        SELECT * FROM users
        WHERE username = '${username}'
        AND password = SHA('${password}')
    `
  
    dbConnection.query(query, (response) => {
        callback(
            false,
            response.results !== null && response.results.length  === 1
                ?  response.results[0]
                : null)
    })
}

function exists(username, callback) {
    const query = `SELECT * FROM users WHERE username = '${username}'`
  
    const dbCallback = (response) => {
        const exists = response.results !== null
            ? response.results.length > 0
                ? true
                : false
            : null
        callback(response.error, exists)
    }
  
    dbConnection.query(query, dbCallback)
  }