const mysql = require('mysql')

module.exports = { query }

let connection

function initConnection () {
    connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '3975',
        database: 'oauth'
    })
}

function query (queryString, callback) {
    initConnection()
    connection.connect()
    connection.query(queryString, function (error, results, fields) {
        console.log({ error, results })
        connection.end()
        callback(formatResult(error, results))
    })
}

function formatResult (error, results) {
    return {
        error,
        results: results == undefined ? null : results
    }
}