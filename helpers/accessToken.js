let dbConnection

module.exports = function (injectedDbConnection) {
    dbConnection = injectedDbConnection

    return {
        saveAccessToken,
        getUserIdFromBearerToken,
    }
}

function saveAccessToken(accessToken, userId, callback) {
    const query = `INSERT INTO access_tokens (access_token, user_id) VALUES ("${accessToken}", "${userId}") ON DUPLICATE KEY UPDATE access_token = "${accessToken}";`
    console.log(query);
    

    dbConnection.query(query, function (response) {
        callback(response.error)
    })
}

function getUserIdFromBearerToken (bearerToken, callback) {
    const query = `
        SELECT * FROM access_tokens WHERE access_token = '${bearerToken}'
    `

    dbConnection.query(query, function (response) {
        let userId

        if (response.result !== null && response.results.length === 1) {
            userId = response.results[0].user_id  
        } else {
            userId = null
        }

        callback(userId)
    })
}