const express = require('express')
const bodyParser = require('body-parser')
const oAuth2Server = require('node-oauth2-server')
const cors = require('cors')

const dbConnection = require('./helpers/mysql')
const accessTokenHelper = require('./helpers/accessToken')(dbConnection)
const userHelper = require('./helpers/user')(dbConnection)
const oAuthModel = require('./authorization/accessTokenModel')(userHelper, accessTokenHelper)

const port = 8000
const app = express()

app.oauth = oAuth2Server({
    model: oAuthModel,
    grants: [
        'password',
        // 'refresh_token',
    ],
    debug: true,
	accessTokenLifetime: 60 * 60 * 24 * 365,
})

app.use(cors())
app.use(app.oauth.errorHandler())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('public'))

// Auth server
app.post('/register', function (request, response) {
    userHelper.exists(request.body.username, function (error, exists) {
        if (error || exists) {
            if (error) {
                response.status(400).json({ error })
            }

            if (exists) {
                response.status(400).json({ error: 'User sudah ada'})
            }

            return
        }

        userHelper.register(
            request.body.username,
            request.body.password,
            function (queryResponse) {
                if (queryResponse.error) {
                    response.status(400).json(error)

                    return
                }

                response.json({ message: 'Registrasi berhasil' })
            }
        )
    })
})
// get access token
app.post('/login', app.oauth.grant())

// Resource server
app.get('/restricted', app.oauth.authorise(), function (request, response) {
    response.send(response.locals.oauth.token.user)
})

app.listen(port, function() {
    console.log(`Listening on port ${port}`)
})